 routes = [
    { server_name='acsacsar-ams-ipv4', via=['anon-dnscrypt.uk-ipv4', 'anon-meganerd'] },
    { server_name='dnscrypt.uk-ipv4', via=['anon-scaleway-ams', 'anon-yofiji-se-ipv4'] },
    { server_name='meganerd', via=['anon-acsacsar-ams-ipv4', 'anon-dnscrypt.uk-ipv4'] },
    { server_name='scaleway-ams', via=['anon-meganerd', 'anon-yofiji-se-ipv4'] },
	{ server_name='yofiji-se-ipv4', via=['anon-acsacsar-ams-ipv4', 'anon-dnscrypt.uk-ipv4'] },
    { server_name='acsacsar-ams-ipv6', via=['anon-yofiji-se-ipv4', 'anon-scaleway-ams-ipv6'] },
    { server_name='dnscrypt.uk-ipv6', via=['anon-meganerd-ipv6', 'anon-acsacsar-ams-ipv6'] },
    { server_name='meganerd-ipv6', via=['anon-yofiji-se-ipv6', 'anon-scaleway-ams-ipv6'] },
    { server_name='scaleway-ams-ipv6', via=['anon-meganerd-ipv6', 'anon-dnscypt.uk-ipv6'] },
    { server_name='yofiji-se-ipv6', via=['anon-acsacsar-ams-ipv6', 'anon-scaleway-ams-ipv6'] }
 ]